VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TGSInspector_Period"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
'Utility class to inspect Trial By Period License Model

Private m_lic As TGSLicense

Friend Property Let License(lic As TGSLicense)
'[INTERNAL]
  Set m_lic = lic
End Property


'============================== Public Properties =================================
Friend Property Get Period() As Long
'Trial Period in seconds
  Period = m_lic.getParamByName("periodInSeconds").Value
End Property

Friend Property Get IsClockRollbackDetectionEnabled() As Boolean
  IsClockRollbackDetectionEnabled = m_lic.getParamByName("rollbackTolerance").Value > 0
End Property

Friend Property Get RollbackTolerance() As Long
'Clock Rollback Tolerance, 0 if disabled
  RollbackTolerance = m_lic.getParamByName("rollbackTolerance").Value
End Property

Friend Property Get IsEntityEverAccessed() As Boolean
'Does the entity protected ever accessed at least once?
  IsEntityEverAccessed = m_lic.getParamByName("timeFirstAccess").HasValue
End Property

Friend Property Get TimeFirstAccess() As Date
'Local Date when the entity is accessed for the first time
  TimeFirstAccess = m_lic.getParamByName("timeFirstAccess").Value
End Property

Friend Property Get ExpireDate() As Date
'When to expire?
  If IsEntityEverAccessed Then
    ExpireDate = DateAdd("s", Period, TimeFirstAccess)
  Else
    'Never accessed before, so we do not know when to expire
    err.Raise vbObjectError + GSErr.VARIABLE_INVALID_VALUE, "Inspector_Period", "ExpireDate is not available!"
  End If
End Property

Friend Property Get TimeUsed() As Long
'How many seconds consumed since first access
  If IsEntityEverAccessed Then
    TimeUsed = DateDiff("s", TimeFirstAccess, Now)
    If TimeUsed > Period Then TimeUsed = Period
  Else
    'Never accessed before, so have full time left
    TimeUsed = 0
  End If
End Property


Friend Property Get TimeLeft() As Long
'How many seconds left before expire
  TimeLeft = Period - TimeUsed
End Property

'============================== Public Methods ====================================


Private Sub Class_Terminate()
  Set m_lic = Nothing
End Sub
