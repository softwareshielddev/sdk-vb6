VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TGSCore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'[INTERNAL ] ******************************* Native Low-Level API *********************************************
Private Declare Function gsInit Lib "gsCore.dll" Alias "#3" (ByVal ProductId As String, ByVal LicenseFile As String, ByVal Password As String, ByVal reserved As Any) As Long
Private Declare Function gsCleanUp Lib "gsCore.dll" Alias "#4" () As Long
Private Declare Sub gsCloseHandle Lib "gsCore.dll" Alias "#5" (ByVal handle As Long)
Private Declare Sub gsFlush Lib "gsCore.dll" Alias "#6" ()

Private Declare Function gsGetLastErrorMessage Lib "gsCore.dll" Alias "#7" () As Long
Private Declare Function gsGetLastErrorCode Lib "gsCore.dll" Alias "#8" () As Long

Private Declare Function gsGetProductName Lib "gsCore.dll" Alias "#84" () As Long
Private Declare Function gsGetProductId Lib "gsCore.dll" Alias "#85" () As Long
Private Declare Function gsGetBuildId Lib "gsCore.dll" Alias "#9" () As Long

'Entity
Private Declare Function gsGetEntityCount Lib "gsCore.dll" Alias "#10" () As Long
Private Declare Function gsOpenEntityByIndex Lib "gsCore.dll" Alias "#11" (ByVal index As Long) As Long
Private Declare Function gsOpenEntityById Lib "gsCore.dll" Alias "#12" (ByVal entityId As String) As Long

'User Defined Variables
Private Declare Function gsGetTotalVariables Lib "gsCore.dll" Alias "#122" () As Long
Private Declare Function gsGetVariableByIndex Lib "gsCore.dll" Alias "#123" (ByVal index As Long) As Long
Private Declare Function gsGetVariableByName Lib "gsCore.dll" Alias "#52" (ByVal varName As String) As Long

'Server
Private Declare Function gsIsServerAlive Lib "gsCore.dll" Alias "#131" (ByVal timeout As Long) As Byte
Private Declare Function gsIsSNValid Lib "gsCore.dll" Alias "#139" (ByVal sn As String, timeout As Long) As Byte
Private Declare Function gsApplySN Lib "gsCore.dll" Alias "#133" (ByVal sn As String, ByRef retCode As Long, ByRef snRef As Long, ByVal timeout As Long) As Byte

Private Declare Function gsApplyLicenseCodeEx Lib "gsCore.dll" Alias "#158" (ByVal liceneCode As String, ByVal sn As String, ByVal snRef As String) As Byte

Private Declare Function gsRevokeApp Lib "gsCore.dll" Alias "#135" (ByVal timeout As Long, ByVal preSN As String) As Byte
Private Declare Function gsRevokeSN Lib "gsCore.dll" Alias "#144" (ByVal timeout As Long, ByVal sn As String) As Byte

Private Declare Function gsMPUploadApp Lib "gsCore.dll" Alias "#152" (ByVal preSN As String, ByVal timeout As Long) As Long
Private Declare Function gsMPExportApp Lib "gsCore.dll" Alias "#153" () As Long

Private Declare Function gsGetPreliminarySN Lib "gsCore.dll" Alias "#155" () As Long

'Request
Private Declare Function gsCreateRequest Lib "gsCore.dll" Alias "#36" () As Long

'Move
Private Declare Function gsMPCreate Lib "gsCore.dll" Alias "#145" (ByVal reserved As Long) As Long
Private Declare Function gsMPOpen Lib "gsCore.dll" Alias "#149" (ByVal mpStr As String) As Long



'[PRIVATE] *********** Private Member Data Area ****************************************************
Private m_cleaned As Boolean ' init() has been called
Private m_entities() As TGSEntity 'Array of entities
Private m_vars() As TGSVariable 'User defined variables


Private Sub Class_Initialize()
    m_cleaned = True
End Sub

Private Sub Class_Terminate()
  cleanUp
End Sub

Friend Sub cleanUp()
'[INTERNAL]
    Dim i As Integer
    
    If Not m_cleaned Then
      Erase m_entities
      Erase m_vars
      
      gsCleanUp
      m_cleaned = True
    End If
End Sub



Friend Function Init(ByVal ProductId As String, ByVal LicenseFile As String, ByVal Password As String) As Integer
'[INTERNAL] Called by TGS.core()
    Dim rc, i, k As Integer
    Dim hEntity, hVar As Long
    
    rc = gsInit(ProductId, LicenseFile, Password, 0&)
    m_cleaned = False
    Init = rc
    
    If rc = 0 Then
        'Entity
        k = gsGetEntityCount()
        If k > 0 Then
            ReDim m_entities(0 To k - 1) As TGSEntity
            
            For i = 0 To k - 1
                hEntity = gsOpenEntityByIndex(i)
                If hEntity = 0 Then
                    err.Raise vbObjectError + GSErr.ENTITY_OPEN_FAILURE, "gs.core", "Entity Open Failure!"
                End If
                
                Set m_entities(i) = New TGSEntity
                m_entities(i).handle = hEntity
            Next
        End If
        
        'Variable
        k = gsGetTotalVariables()
        If k > 0 Then
            ReDim m_vars(0 To k - 1) As TGSVariable
            For i = 0 To k - 1
              hVar = gsGetVariableByIndex(i)
              If hVar <> 0 Then
                Set m_vars(i) = New TGSVariable
                m_vars(i).handle = hVar
              End If
            Next
        End If
    End If
    
End Function



Friend Sub closeHandle(ByVal handle As Long)
'[INTERNAL]
  Call gsCloseHandle(handle)
End Sub


Friend Sub flush()
'[INTERNAL]
  Call gsFlush
End Sub



'******************************* Properties *****************************************************************
Friend Property Get LastErrorCode() As Integer
  LastErrorCode = gsGetLastErrorCode()
End Property

Friend Property Get LastErrorMessage() As String
  LastErrorMessage = PCharToStr(gsGetLastErrorMessage())
End Property
Friend Property Get ProductName() As String
'Product Name stored in local license storage
  ProductName = PCharToStr(gsGetProductName())
End Property
Friend Property Get ProductId() As String
'Product Id stored in local license storage
  ProductId = PCharToStr(gsGetProductId())
End Property

Friend Property Get BuildId() As Integer
'Current Build-Id stored in local license storage
  BuildId = gsGetBuildId()
End Property

Friend Property Get EntityCount() As Integer
'Total number of pre-defined entitities in license project
    EntityCount = UBound(m_entities) - LBound(m_entities) + 1
End Property

Friend Property Get Entities(ByVal index As Integer) As TGSEntity
    Set Entities = m_entities(index)
End Property

Friend Function getEntityByIndex(ByVal index As Integer) As TGSEntity
    Set getEntityByIndex = m_entities(index)
End Function

Friend Function getEntityByHandle(ByVal hEntity As Long) As TGSEntity
'[INTERNAL]
  Dim i As Integer
  
  Set getEntityByHandle = Nothing
  
  For i = LBound(m_entities) To UBound(m_entities)
    If m_entities(i).handle = hEntity Then
      Set getEntityByHandle = m_entities(i)
      Exit For
    End If
  Next
End Function

Friend Function getEntityById(ByVal entityId As String) As TGSEntity
  Dim i As Integer
  
  Set getEntityById = Nothing
  
  For i = LBound(m_entities) To UBound(m_entities)
    If m_entities(i).id = entityId Then
      Set getEntityById = m_entities(i)
      Exit For
    End If
  Next
End Function


Friend Property Get VariableCount() As Integer
'Total number of pre-defined variables in license project
    VariableCount = UBound(m_vars) - LBound(m_vars) + 1
End Property

Friend Property Get Variables(ByVal index As Integer) As TGSVariable
    Set Variables = m_vars(index)
End Property

Friend Function createRequest() As TGSRequest
  Dim hReq As Long
  hReq = gsCreateRequest()
  If hReq <> 0 Then
    Set createRequest = New TGSRequest
    createRequest.handle = hReq
  Else
    err.Raise vbObjectError + GSErr.REQUEST_CREATE_FAILURE, "gs.core", "Request create failure!"
  End If
End Function

'****************************** Public Methods ***********************************************
Friend Function getVariableByName(varName As String) As TGSVariable
'Get variable by name
    Dim hVar As Long
    hVar = gsGetVariableByName(varName)
    If hVar <> 0 Then
      Set getVariableByName = New TGSVariable
      getVariableByName.handle = hVar
    Else
      err.Raise vbObjectError + GSErr.VARIABLE_NOT_FOUND, "gs.core", "Variable [" & varName & "] not found!"
    End If
End Function

'******************** Activation ************************
Friend Property Get IsServerAlive() As Boolean
'Test if the license server is alive
  IsServerAlive = gsIsServerAlive(-1) <> 0
End Property

Friend Function applyLicenseCode(ByVal licenseCode As String, Optional ByVal sn As String = "") As Boolean
  applyLicenseCode = gsApplyLicenseCodeEx(licenseCode, sn, "") <> 0
End Function


Friend Function applySN(ByVal sn As String, Optional ByRef retCode As Long = 0&) As Boolean
  Dim pSNRef As Long
  
  If retCode = 0 Then
    applySN = gsApplySN(sn, 0&, pSNRef, -1) <> 0
  Else
    applySN = gsApplySN(sn, retCode, pSNRef, -1) <> 0
  End If
End Function

Friend Function isValidSN(ByVal sn As String) As Boolean
  isValidSN = gsIsSNValid(sn, -1) <> 0
End Function

Friend Sub lockAllEntities()
  Dim i As Integer
  
  For i = LBound(m_entities) To UBound(m_entities)
    m_entities(i).License.deactivate
  Next
End Sub

Private Function getRequestCode(ByVal actId As Byte) As String
  Dim req As TGSRequest
  Dim act As TGSAction
  
  Set req = createRequest()
  Set act = req.addAction(actId)
  
  getRequestCode = req.Code
  
  Set req = Nothing
  Set act = Nothing
End Function


Friend Property Get DummyRequestCode() As String
  DummyRequestCode = getRequestCode(ACT_DUMMY)
End Property

Friend Property Get UnlockRequestCode() As String
  UnlockRequestCode = getRequestCode(ACT_UNLOCK)
End Property

Friend Property Get CleanRequestCode() As String
  CleanRequestCode = getRequestCode(ACT_CLEAN)
End Property

Friend Property Get FixRequestCode() As String
  FixRequestCode = getRequestCode(ACT_FIX)
End Property

'**************** Move *********************
Friend Function revokeApp(Optional ByVal preSN As String = "") As Boolean
  revokeApp = gsRevokeApp(-1, preSN) <> 0
End Function

Friend Function revokeSN(ByVal sn As String) As Boolean
  revokeSN = gsRevokeSN(-1, sn) <> 0
End Function

Friend Function uploadApp(Optional ByVal preSN As String = "") As String
  uploadApp = PCharToStr(gsMPUploadApp(preSN, -1))
End Function

Friend Function exportApp() As String
  exportApp = PCharToStr(gsMPExportApp())
End Function

Friend Function createMovePackage(Optional ByVal mvDataStr As String = "") As TMovePackage
  Dim hMP As Long
  
  If mvDataStr = "" Then
    hMP = gsMPCreate(0)
  Else
    hMP = gsMPOpen(mvDataStr)
  End If
  
  If hMP = 0 Then
    err.Raise vbObjectError + GSErr.MVPKG_CREATE_FAILURE, "gs.core", "Move Package create failure!"
  End If
  
  createMovePackage = New TMovePackage
  createMovePackage.handle = hMP
End Function

