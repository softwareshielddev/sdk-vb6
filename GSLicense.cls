VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TGSLicense"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public Enum GSLicenseModelKind
    LM_UNKNOWN = 0
    
    LM_EXPIRE_PERIOD = 1
    LM_EXPIRE_DURATION = 2
    LM_EXPIRE_HARDDATE = 3
    LM_EXPIRE_ACCESSTIME = 4
    LM_EXPIRE_SESSIONTIME = 5
    LM_ALWAYS_LOCK = 6
    LM_ALWAYS_RUN = 7
End Enum

Private m_hLicense As Long

Private m_inspector As Object 'License Model Specific Inspector

Private m_params() As TGSVariable 'License Model Parameters


'[INTERNAL ] ******************************* Native Low-Level API *********************************************

Private Declare Function gsGetLicenseStatus Lib "gsCore.dll" Alias "#24" (ByVal hLicense As Long) As Long
Private Declare Function gsGetLicenseId Lib "gsCore.dll" Alias "#28" (ByVal hLicense As Long) As Long
Private Declare Function gsGetLicenseName Lib "gsCore.dll" Alias "#22" (ByVal hLicense As Long) As Long
Private Declare Function gsGetLicenseDescription Lib "gsCore.dll" Alias "#23" (ByVal hLicense As Long) As Long
Private Declare Function gsIsLicenseValid Lib "gsCore.dll" Alias "#34" (ByVal hLicense As Long) As Byte
Private Declare Sub gsLockLicense Lib "gsCore.dll" Alias "#138" (ByVal hLicense As Long)
Private Declare Function gsGetLicenseParamCount Lib "gsCore.dll" Alias "#29" (ByVal hLicense As Long) As Long
Private Declare Function gsGetLicenseParamByIndex Lib "gsCore.dll" Alias "#30" (ByVal hLicense As Long, ByVal index As Long) As Long
Private Declare Function gsGetLicenseParamByName Lib "gsCore.dll" Alias "#31" (ByVal hLicense As Long, ByVal varName As String) As Long


'============================== Internal Properties ====================================
Friend Property Let handle(hLicense As Long)
'[INTERNAL]
  Dim i, N As Integer
  Dim hParam As Long
  
  m_hLicense = hLicense
  
  'Retrieve all parameters
  N = gsGetLicenseParamCount(m_hLicense)
  If N > 0 Then
    ReDim m_params(0 To N - 1) As TGSVariable
    
    For i = 0 To N - 1
       hParam = gsGetLicenseParamByIndex(m_hLicense, i)
       If hParam = 0 Then
           err.Raise vbObjectError + GSErr.LICENSEPARAM_OPEN_FAILURE, "TGSLicense", "License Parameter Open Failure!"
       End If
       
       Set m_params(i) = New TGSVariable
       m_params(i).handle = hParam
    Next
  End If
End Property

Friend Property Get handle() As Long
'[INTERNAL]
  handle = m_hLicense
End Property

' In GS5, we can lock a license from code explicitly, but cannot unlock it without applying an authorized action
Friend Sub deactivate()
  Call gsLockLicense(m_hLicense)
End Sub




'============================== Public Properties ====================================

Friend Property Get Name() As String
  Name = PCharToStr(gsGetLicenseName(m_hLicense))
End Property

Friend Property Get Kind() As GSLicenseModelKind
'Built-in License Model Type

  Dim id As String
  id = PCharToStr(gsGetLicenseId(m_hLicense))
  
  Select Case id
    Case "gs.lm.expire.period.1"
      Kind = LM_EXPIRE_PERIOD
      
    Case "gs.lm.expire.duration.1"
      Kind = LM_EXPIRE_DURATION
      
    Case "gs.lm.expire.hardDate.1"
      Kind = LM_EXPIRE_HARDDATE
      
    Case "gs.lm.expire.accessTime.1"
      Kind = LM_EXPIRE_ACCESSTIME
      
    Case "gs.lm.expire.sessionTime.1"
      Kind = LM_EXPIRE_SESSIONTIME
      
    Case "gs.lm.alwaysLock.1"
      Kind = LM_ALWAYS_LOCK
    
    Case "gs.lm.alwaysRun.1"
      Kind = LM_ALWAYS_RUN
        
    Case Else
      Kind = LM_UNKNOWN
  End Select
End Property


Friend Property Get Description() As String
  Description = PCharToStr(gsGetLicenseDescription(m_hLicense))
End Property

Friend Property Get IsValid() As Boolean
'The License is in a valid status (either activated or still in trial mode), the protected entity can be accessed right now
  IsValid = (gsIsLicenseValid(m_hLicense) <> 0)
End Property

Friend Property Get IsTrialMode() As Boolean
'The License is still running in trial mode (not activated and not expired yet)
  IsTrialMode = IsValid And Not IsActivated
End Property


Friend Property Get IsActivated() As Boolean
'License is fully activated / unlocked
  IsActivated = (gsGetLicenseStatus(m_hLicense) = 1)
End Property

Friend Property Get IsDeactivated() As Boolean
'License is already locked (or expired since it gets locked automatically when expires)
  IsDeactivated = (gsGetLicenseStatus(m_hLicense) = 0)
End Property

Friend Property Get IsExpired() As Boolean
'License is expired, it gets locked automatically when expires
  IsExpired = IsDeactivated
End Property

Friend Property Get ParamCount() As Integer
'Get total license parameter count
    ParamCount = UBound(m_params) - LBound(m_params) + 1
End Property

Friend Property Get Params(index As Integer) As TGSVariable
'Get indexed license parameter
    Set Params = m_params(index)
End Property

Friend Function getParamByName(paramName As String) As TGSVariable
'Get license parameter by its name, raise exception if not found
    Dim i As Integer
    
    For i = 0 To ParamCount - 1
        If m_params(i).Name = paramName Then
            Set getParamByName = m_params(i)
            Exit Function
        End If
    Next
    
    err.Raise vbObjectError + GSErr.VARIABLE_NOT_FOUND, "TGSLicense", "License Parameter (" & paramName & ") not found!"
End Function

Friend Property Get inspector() As Object
'License specific inspector
  
  If m_inspector Is Nothing Then
    Select Case Kind
      Case LM_EXPIRE_PERIOD
        Dim Period As TGSInspector_Period
        
        Set Period = New TGSInspector_Period
        Period.License = Me
        Set m_inspector = Period
      
      Case LM_EXPIRE_DURATION
        Dim Duration As TGSInspector_Duration
        
        Set Duration = New TGSInspector_Duration
        Duration.License = Me
        Set m_inspector = Duration
        
      Case LM_EXPIRE_HARDDATE
        Dim hardDate As TGSInspector_HardDate
        
        Set hardDate = New TGSInspector_HardDate
        hardDate.License = Me
        Set m_inspector = hardDate
        
      Case LM_EXPIRE_ACCESSTIME
        Dim accessTime As TGSInspector_AccessTime
        
        Set accessTime = New TGSInspector_AccessTime
        accessTime.License = Me
        Set m_inspector = accessTime
        
      Case LM_EXPIRE_SESSIONTIME
        Dim sessionTime As TGSInspector_SessionTime
        
        Set sessionTime = New TGSInspector_SessionTime
        sessionTime.License = Me
        Set m_inspector = sessionTime
        
      Case Else
        'Place holder
        Set m_inspector = New TGSInspector
    End Select
  End If
  
  Set inspector = m_inspector
End Property




Private Sub Class_Terminate()
  Erase m_params
  Set m_inspector = Nothing
  Call gsCloseHandle(m_hLicense)
End Sub

