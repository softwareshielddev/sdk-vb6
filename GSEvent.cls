VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TGSEvent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Enum GSEventType
  EVENT_TYPE_APP = 0 ' Application Event Id Range: [0, 99)
  EVENT_TYPE_LICENSE = 100 ' License Event Id Range: [100, 199)
  EVENT_TYPE_ENTITY = 200 'Entity Event Id Range: [200, 299)
  EVENT_TYPE_USER = &H10000000  ' User Defined Event Id Range: [0x10000000, 0xFFFFFFFF)
End Enum

'Error Event Type
Public Enum GSErrorType
  WARN_CLOCK_ROLLBACK = 1
  WARN_INTEGRITY_CORRUPT = 2
  ERROR_FINGERPRINT_MISMATCH = 3
  ERROR_LICENSE_FAIL = 4
End Enum

Public Enum GSEventId
  EVENT_APP_BEGIN = 1             'Application just gets started, the local license has been connected,
  EVENT_APP_RUN = 5               'Application starts to run, last signal before game code is executing
  EVENT_APP_END = 2               'Application is going to terminate, last signal before game exits.
  
  EVENT_APP_CLOCK_ROLLBACK = 3    'Alarm: Application detects the clock is rolled back
  EVENT_APP_INTEGRITY_CORRUPT = 4 'Alarm:  Application integrity is corrupted.
  
  EVENT_LICENSE_NEWINSTALL = 101  'Application launchs for the first time after installation
  EVENT_LICENSE_LOADING = 105     'License is loading from local storage
  EVENT_LICENSE_FAIL = 103        'Error: The application's license store cannot be connected /initialized!
  EVENT_LICENSE_READY = 102       'The application's license store is connected /initialized successfully

  'The entity is to be accessed.
  'The event listeners can check license status and pops up UI to activate or modify the license as needed.
  EVENT_ENTITY_ACCESS_STARTING = 201
  ' The entity is being accessed.
  'The listeners can enable any protected resources here. (injecting decrypt keys, etc.)
  EVENT_ENTITY_ACCESS_STARTED = 202
  
  '
  ' The entity access is ending now.
  ' The listeners can revoke any protected resources here. (remove injected decrypting keys, etc.)
  EVENT_ENTITY_ACCESS_ENDING = 203

  ' The entity access is ended
  EVENT_ENTITY_ACCESS_ENDED = 204

  'Alarm: Entity access invalid (due to expiration, etc)
  'The listeners can pops up UI to activate the license, or terminate app immediately
  EVENT_ENTITY_ACCESS_INVALID = 205
  
  'Internal ping event indicating entity is still alive (being accessed).
  'The listeners can check license status and display in-app remainder message if the license is going to expire
  EVENT_ENTITY_ACCESS_HEARTBEAT = 206
  
  ' Action Applied to Entity
  ' The status of attached license have been modified by applying license action.
  ' It is called after the change has been made.
  EVENT_ENTITY_ACTION_APPLIED = 208
End Enum



