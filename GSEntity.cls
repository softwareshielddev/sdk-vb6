VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TGSEntity"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_hEntity As Long
Private m_license As TGSLicense
'[INTERNAL ] ******************************* Native Low-Level API *********************************************

Private Declare Function gsGetEntityAttributes Lib "gsCore.dll" Alias "#13" (ByVal hEntity As Long) As Long
Private Declare Function gsGetEntityId Lib "gsCore.dll" Alias "#14" (ByVal hEntity As Long) As Long
Private Declare Function gsGetEntityName Lib "gsCore.dll" Alias "#15" (ByVal hEntity As Long) As Long
Private Declare Function gsGetEntityDescription Lib "gsCore.dll" Alias "#16" (ByVal hEntity As Long) As Long

Private Declare Function gsBeginAccessEntity Lib "gsCore.dll" Alias "#20" (ByVal hEntity As Long) As Byte
Private Declare Function gsEndAccessEntity Lib "gsCore.dll" Alias "#21" (ByVal hEntity As Long) As Byte

Private Declare Function gsOpenLicense Lib "gsCore.dll" Alias "#137" (ByVal hEntity As Long) As Long



'============================== Internal Properties ====================================
Friend Property Let handle(hEntity As Long)
'[INTERNAL]

  m_hEntity = hEntity
  
  Set m_license = New TGSLicense
  m_license.handle = gsOpenLicense(m_hEntity)
End Property

Friend Property Get handle() As Long
'[INTERNAL]
  handle = m_hEntity
End Property


'============================== Public Properties ====================================

Friend Property Get Name() As String
  Name = PCharToStr(gsGetEntityName(m_hEntity))
End Property

Friend Property Get id() As String
  id = PCharToStr(gsGetEntityId(m_hEntity))
End Property

Friend Property Get Description() As String
  Description = PCharToStr(gsGetEntityDescription(m_hEntity))
End Property

Friend Property Get IsAutoStart() As Boolean
'AutoStart Entity: If wrapped, the BeginAccess() is called automatically by SoftwareShield Runtime when app launching
  IsAutoStart = gsGetEntityAttributes(m_hEntity) And &H10
End Property

Friend Property Get License() As TGSLicense
  Set License = m_license
End Property

Friend Property Get Unlocked() As Boolean
  Unlocked = License.IsActivated
End Property

Friend Property Get Locked() As Boolean
  Locked = License.IsDeactivated
End Property


'============================== Public Methods ====================================
Friend Function BeginAccess() As Boolean
  BeginAccess = gsBeginAccessEntity(m_hEntity) <> 0
End Function

Friend Function EndAccess() As Boolean
  EndAccess = gsEndAccessEntity(m_hEntity) <> 0
End Function


Private Sub Class_Terminate()
  Set m_license = Nothing
  Call gsCloseHandle(m_hEntity)
End Sub
