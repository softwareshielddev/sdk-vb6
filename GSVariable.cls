VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TGSVariable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public Enum TVarType
    VT_STRING = 1
    VT_BOOLEAN = 2
    VT_LONG = 3
    VT_FLOAT = 4
    VT_DATE = 5
End Enum

Private m_hVar As Long
'[INTERNAL ] ******************************* Native Low-Level API *********************************************

Private Declare Function gsGetVariableName Lib "gsCore.dll" Alias "#53" (ByVal hVar As Long) As Long
Private Declare Function gsGetVariableTypeId Lib "gsCore.dll" Alias "#54" (ByVal hVar As Long) As Byte
Private Declare Function gsGetVariableValueAsString Lib "gsCore.dll" Alias "#57" (ByVal hVar As Long) As Long
Private Declare Function gsIsVariableValid Lib "gsCore.dll" Alias "#67" (ByVal hVar As Long) As Byte

Private Declare Function gsSetVariableValueFromString Lib "gsCore.dll" Alias "#58" (ByVal hVar As Long, ByVal str As String) As Byte
Private Declare Function gsGetVariableValueAsFileTime Lib "gsCore.dll" Alias "#165" (ByVal hVar As Long, ByRef ft As FILETIME) As Byte
Private Declare Function gsSetVariableValueFromFileTime Lib "gsCore.dll" Alias "#166" (ByVal hVar As Long, ByRef ft As FILETIME) As Byte



'============================== Internal Properties ====================================
Friend Property Let handle(ByVal hVar As Long)
  m_hVar = hVar
End Property

Friend Property Get handle() As Long
  handle = m_hVar
End Property
'============================== Public Properties ====================================
Friend Property Get Name() As String
    Name = PCharToStr(gsGetVariableName(m_hVar))
End Property

Friend Property Get TypeName() As String
  Select Case Me.typeId
    Case VT_LONG
        TypeName = "Long"
    Case VT_FLOAT
        TypeName = "Float"
    Case VT_BOOLEAN
        TypeName = "Boolean"
    Case VT_DATE
        TypeName = "Date"
    Case VT_STRING
        TypeName = "String"
  End Select
End Property

Friend Property Get typeId() As TVarType
    Dim id As Byte
    
    id = gsGetVariableTypeId(m_hVar)
    Select Case id
        Case 1, 2, 3, 4, 5, 6, 7, 8
          typeId = VT_LONG
        Case 9, 10
          typeId = VT_FLOAT
        Case 11
          typeId = VT_BOOLEAN
        Case 30
          typeId = VT_DATE
        Case Else
        typeId = VT_STRING
    End Select
End Property

Friend Property Get ValAsStr() As String
'Variable value in string format
  ValAsStr = PCharToStr(gsGetVariableValueAsString(m_hVar))
End Property

Friend Property Let ValAsStr(v As String)
'Set Variable value in string format
  If gsSetVariableValueFromString(m_hVar, v) = 0 Then
    err.Raise vbObjectError + GSErr.VARIABLE_INVALID_VALUE, "TGSVariable", "Invalid Value!"
  End If
End Property


Friend Property Get ValAsUTCDate() As Date
'Variable value in UTC Date
    Dim utc_file_time As FILETIME
    Dim system_time As SYSTEMTIME
    
    If gsGetVariableValueAsFileTime(m_hVar, utc_file_time) <> 0 Then
        ' Convert it to a SYSTEMTIME.
        FileTimeToSystemTime utc_file_time, system_time
        ' Convert it to a Date.
        SystemTimeToDate system_time, ValAsUTCDate
    Else
        err.Raise vbObjectError + GSErr.VARIABLE_INVALID_VALUE, "TGSVariable", "Invalid Date Value!"
    End If
End Property

Friend Property Let ValAsUTCDate(v As Date)
    Dim utc_file_time As FILETIME
    Dim system_time As SYSTEMTIME
    
    DateToSystemTime v, system_time
    SystemTimeToFileTime system_time, utc_file_time
    
    If gsSetVariableValueFromFileTime(m_hVar, utc_file_time) = 0 Then
      err.Raise vbObjectError + GSErr.VARIABLE_INVALID_VALUE, "TGSVariable", "Invalid Date Value!"
    End If
End Property

Friend Property Get ValAsLocalDate() As Date
'Get Local Date
    ValAsLocalDate = UTCToLocalTime(Me.ValAsUTCDate)
End Property

Friend Property Let ValAsLocalDate(v As Date)
'Set Local Date
    ValAsUTCDate = LocalTimeToUTC(v)
End Property


Friend Property Get ValAsLong() As Long
'Get Long Value
    ValAsLong = CLng(ValAsStr)
End Property

Friend Property Let ValAsLong(v As Long)
'Set Long Value
    ValAsStr = CStr(v)
End Property

Friend Property Get ValAsSingle() As Single
'Get Single Value
    ValAsSingle = CSng(ValAsStr)
End Property

Friend Property Let ValAsSingle(v As Single)
'Set Single Value
    ValAsStr = CStr(v)
End Property

Friend Property Get ValAsBoolean() As Boolean
'Get Boolean Value
    ValAsBoolean = CBool(ValAsStr)
End Property

Friend Property Let ValAsBoolean(v As Boolean)
'Set Boolean Value
    ValAsStr = CStr(v)
End Property

Friend Property Get Value() As Variant
  If HasValue Then
    Select Case Me.typeId
      Case VT_LONG
          Value = ValAsLong
      Case VT_FLOAT
          Value = ValAsSingle
      Case VT_BOOLEAN
          Value = ValAsBoolean
      Case VT_DATE
          Value = ValAsLocalDate
      Case VT_STRING
          Value = ValAsStr
    End Select
  Else
    Value = Null
  End If
End Property

Friend Property Let Value(v As Variant)
  Select Case Me.typeId
    Case VT_LONG
        ValAsLong = v
    Case VT_FLOAT
        ValAsSingle = v
    Case VT_BOOLEAN
        ValAsBoolean = v
    Case VT_DATE
        ValAsLocalDate = v
    Case VT_STRING
        ValAsStr = v
  End Select
End Property


Friend Property Get HasValue()
'Test if the variable holds a valid value.
'A variable's value might not have been initialized, for example, the time_first_access property of LM_ExpireByPeriod might not be initialized until the property
'is accessed once
  HasValue = gsIsVariableValid(m_hVar) <> 0
End Property




Private Sub Class_Terminate()
  Call gsCloseHandle(m_hVar)
End Sub
