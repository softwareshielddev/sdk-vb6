Attribute VB_Name = "TGSIntf"
'One and only Root Object of GS/SS SDK, can only be accessed after calling GSInit
Public gs As TGS


'[INTERNAL ] ******************************* Native Low-Level API *********************************************
Private Declare Function gsStrLen Lib "gsCore.dll" Alias "#163" (ByVal pchar As Long) As Integer
Private Declare Sub gsStrCopy Lib "gsCore.dll" Alias "#164" (ByVal desStr As String, ByVal srcPChar As Long, ByVal desStrLen As Long)
Public Declare Sub gsCloseHandle Lib "gsCore.dll" Alias "#5" (ByVal handle As Long)


 

Public Sub SdkInitialize(Optional ByVal ProductId As String = "", Optional ByVal licFilePath As String = "", Optional ByVal Password As String = "")
    If gs Is Nothing Then
        Set gs = New TGS
        
        gs.ProductId = ProductId
        gs.Password = Password
        gs.LicenseFile = licFilePath
        
        '---------------------------------------------------------
        'make sure the gsCore.dll is side by side with the app exe
        'otherwise VB runtime won't find it
        '
        'you can also do it by calling Windows SetDllDirectory() api
        '---------------------------------------------------------
        ChDir App.Path
        
        'Initialize the app instance first
        gs.App.Init
        
    End If
End Sub

Public Sub SdkUninitialize()
    If Not gs Is Nothing Then
        gs.cleanUp
        
        Set gs = Nothing
    End If
End Sub

    

'[INTERNAL] Helper to convert const char* from C++ dll to string
Public Function PCharToStr(ByVal pchar As Long) As String
 Dim result As String
 Dim msgLen As Integer
 msgLen = gsStrLen(pchar)
 result = Space$(msgLen)
 
 Call gsStrCopy(result, pchar, msgLen)
 PCharToStr = result
End Function

Public Sub s_monitorCallback(ByVal eventId As Long, ByVal hEvent As Long, ByVal usrData As Long)
'[INTERNAL] event call back for all events
  Call gs.App.OnEventCallback(eventId, hEvent)
End Sub



