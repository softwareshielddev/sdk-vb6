VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TGSRequest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'Request Code
Private m_hRequest As Long
'[INTERNAL ] ******************************* Native Low-Level API *********************************************
Private Declare Function gsAddRequestAction Lib "gsCore.dll" Alias "#37" (ByVal hReq As Long, ByVal actId As Byte, ByVal hLic As Long) As Long
Private Declare Function gsGetRequestCode Lib "gsCore.dll" Alias "#45" (ByVal hReq As Long) As Long


'============================== Internal Properties ====================================
Friend Property Let handle(ByVal hRequest As Long)
  m_hRequest = hRequest
End Property

Friend Property Get handle() As Long
  handle = m_hRequest
End Property


Private Sub Class_Terminate()
  Call gsCloseHandle(m_hRequest)
End Sub

Friend Function addAction(ByVal actId As Byte, Optional entity As TGSEntity = Nothing) As TGSAction
  Dim hAct As Long
  
  If entity Is Nothing Then
    hAct = gsAddRequestAction(m_hRequest, actId, 0)
  Else
    hAct = gsAddRequestAction(m_hRequest, actId, entity.License.handle)
  End If
  
  If hAct <> 0 Then
    Set addAction = New TGSAction
    addAction.handle = hAct
  Else
    err.Raise vbObjectError + GSErr.ACTION_CREATE_FAILURE, "TGSRequest.addAction", "Action create failure!"
  End If
End Function

Friend Property Get Code() As String
  Code = PCharToStr(gsGetRequestCode(m_hRequest))
End Property
