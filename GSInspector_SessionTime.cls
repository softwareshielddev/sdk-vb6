VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TGSInspector_SessionTime"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_lic As TGSLicense

Friend Property Let License(lic As TGSLicense)
'[INTERNAL]
  Set m_lic = lic
End Property


'============================== Public Properties =================================
Friend Property Get MaxSessionTime() As Integer
'Maximum allowed time length (in seconds) for each launch session
  MaxSessionTime = m_lic.getParamByName("maxSessionTime").ValAsLong
End Property

Friend Property Get TimeUsed() As Integer
'Elapsed time in seconds) for current launch session
  TimeUsed = m_lic.getParamByName("sessionTimeUsed").ValAsLong
End Property

Friend Property Get TimeLeft() As Long
'How many seconds left before current session expires
  TimeLeft = MaxSessionTime - TimeUsed
End Property

Private Sub Class_Terminate()
  Set m_lic = Nothing
End Sub
