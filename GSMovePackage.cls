VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TMovePackage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'License Move Package
Private m_hMP As Long
'[INTERNAL ] ******************************* Native Low-Level API *********************************************
Private Declare Sub gsMPAddEntity Lib "gsCore.dll" Alias "#146" (ByVal hMP As Long, ByVal entityId As String)
Private Declare Function gsMPExport Lib "gsCore.dll" Alias "#147" (ByVal hMP As Long) As Long

Private Declare Function gsMPIsTooBigToUpload Lib "gsCore.dll" Alias "#157" (ByVal hMP As Long) As Byte
Private Declare Function gsMPUpload Lib "gsCore.dll" Alias "#148" (ByVal hMP As Long, ByVal preSN As String, ByVal timeout As Long) As Long

Private Declare Function gsMPCanPreliminarySNResolved Lib "gsCore.dll" Alias "#156" (ByVal hMP As Long) As Byte

Private Declare Function gsMPImportOnline Lib "gsCore.dll" Alias "#141" (ByVal hMP As Long, ByVal preSN As String, ByVal timeout As Long) As Byte

Private Declare Function gsMPGetImportOfflineRequestCode Lib "gsCore.dll" Alias "#150" (ByVal hMP As Long) As Long
Private Declare Function gsMPImportOffline Lib "gsCore.dll" Alias "#151" (ByVal hMP As Long, ByVal licenseCode As String) As Byte




'============================== Internal Properties ====================================
Friend Property Let handle(ByVal hMP As Long)
  m_hMP = hMP
End Property

Friend Property Get handle() As Long
  handle = m_hMP
End Property


Private Sub Class_Terminate()
  Call gsCloseHandle(m_hMP)
End Sub

Friend Sub addEntityId(ByVal entityId As String)
  Call gsMPAddEntity(m_hMP, entityId)
End Sub

Friend Function canPreliminarySNResolved() As Boolean
  canPreliminarySNResolved = gsMPCanPreliminarySNResolved(m_hMP) <> 0
End Function

' ------- Move License Online --------
' Returns a receipt ( actually a SN ) from server on success
'
' It will be used to activate app on the target machine so
' should be saved in a safely place.
'
' After this api returns, the entities in this move package are locked.
'
Friend Function upload(Optional ByVal preSN As String = "") As String
  If (preSN <> "") Or canPreliminarySNResolved() Then
    upload = PCharToStr(gsMPUpload(m_hMP, preSN, -1))
  Else
    err.Raise vbObjectError + GSErr.MVPKG_UPLOAD_FAILURE, "TMovePackage.upload", "Preliminary Serial must be available for uploading!"
  End If
End Function

Friend Function isTooBigToUpload() As Boolean
  isTooBigToUpload = gsMPIsTooBigToUpload(m_hMP) <> 0
End Function

'----- Move License Offline ---------
' Returns encrypted data string of move package
' It will be used to activate app on the target machine so
' should be saved in a safely place.
'
' On Success:
'   return non-empty string, and the entities in this move package are locked.
'
Friend Function exportData() As String
  exportData = PCharToStr(gsMPExport(m_hMP))
End Function

Friend Function getImportOfflineRequestCode() As String
  getImportOfflineRequestCode = PCharToStr(gsMPGetImportOfflineRequestCode(m_hMP))
End Function

Friend Function importOffline(ByVal licenseCode As String) As Boolean
  importOffline = gsMPImportOffline(m_hMP, licenseCode) <> 0
End Function

Friend Function importOnline(Optional ByVal preSN As String = "") As Boolean
  ' make sure we have a valid preliminary SN for online operation
  If (preSN <> "") Or canPreliminarySNResolved() Then
    importOnline = gsMPImportOnline(m_hMP, preSN, -1)
  Else
    importOnline = False
  End If
End Function




