VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TGSInspector_AccessTime"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_lic As TGSLicense

Friend Property Let License(lic As TGSLicense)
'[INTERNAL]
  Set m_lic = lic
End Property


'============================== Public Properties =================================
Friend Property Get MaxAccessTime() As Integer
'Maximum access time
  MaxAccessTime = m_lic.getParamByName("maxAccessTimes").ValAsLong
End Property


Friend Property Get TimeUsed() As Integer
'How many time is consumed
  TimeUsed = m_lic.getParamByName("usedTimes").ValAsLong
End Property


Friend Property Get TimeLeft() As Integer
'How many time left before expire
  TimeLeft = MaxAccessTime - TimeUsed
  If TimeLeft < 0 Then TimeLeft = 0
End Property

Private Sub Class_Terminate()
  Set m_lic = Nothing
End Sub

