VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TGSInspector_Duration"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_lic As TGSLicense

Friend Property Let License(lic As TGSLicense)
'[INTERNAL]
  Set m_lic = lic
End Property


'============================== Public Properties =================================
Friend Property Get Duration() As Long
'Trial Duration in seconds
  Duration = m_lic.getParamByName("maxDurationInSeconds").Value
End Property


Friend Property Get TimeUsed() As Long
'Trial Duration has been used in seconds
  TimeUsed = m_lic.getParamByName("usedDurationInSeconds").Value
  If TimeUsed > Duration Then TimeUsed = Duration
End Property


Friend Property Get TimeLeft() As Long
'Trial Duration left before expire in seconds
  TimeLeft = Duration - TimeUsed
End Property


Private Sub Class_Terminate()
  Set m_lic = Nothing
End Sub

