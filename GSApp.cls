VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TGSApp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'***************** Public Events ****************************
'Generic event handler
Public Event OnEvent(ByVal eventId As Long)

Public Event OnAppBegin()
Public Event OnAppRun()
Public Event OnAppExit()
Public Event OnAppFirstRun()

Public Event OnLicenseLoading()
Public Event OnLicenseLoaded()


Public Event OnAccessStarting(entity As TGSEntity)
Public Event OnAccessStarted(entity As TGSEntity)

Public Event OnAccessEnding(entity As TGSEntity)
Public Event OnAccessEnded(entity As TGSEntity)

Public Event OnAccessInvalid(entity As TGSEntity)
Public Event OnAccessHeartBeat(entity As TGSEntity)

Public Event OnUserEvent(ByVal eventId As Long, ByVal usrDataPtr As Long, ByVal usrDataSize As Long)

'Error handler
Public Event OnError(ByVal err As GSErrorType, ByVal errCode As Long, ByVal errMsg As String)

'[INTERNAL ] ******************************* Native Low-Level API *********************************************

'Event Callback
Private Declare Function gsCreateMonitorEx Lib "gsCore.dll" Alias "#90" (ByVal addressOfEventCallback As Long, ByVal usrDate As Long, ByVal monitorName As String) As Long
Private Declare Function gsGetEventId Lib "gsCore.dll" Alias "#86" (ByVal hEvent As Long) As Long
Private Declare Function gsGetEventType Lib "gsCore.dll" Alias "#87" (ByVal hEvent As Long) As Long
Private Declare Function gsGetEntityEventSource Lib "gsCore.dll" Alias "#88" (ByVal hEvent As Long) As Long
Private Declare Function gsGetUserEventData Lib "gsCore.dll" Alias "#124" (ByVal hEvent As Long, ByRef usrDataSize As Long) As Long
Private Declare Function gsGetLicenseErrorEventDetails Lib "gsCore.dll" Alias "#167" (ByVal hEvent As Long, ByRef errorCode As Long) As Long


Private m_hMonitorCB As Long 'registered monitor callback handle


Public Sub OnEventCallback(ByVal eventId As Long, ByVal hEvent As Long)
'[INTERNAL]
  Dim errCode As Long
  Dim errMsg As String
  Dim hEntity As Long
  Dim entity As TGSEntity
  
  Dim usrDataSize As Long
  Dim usrDataPtr As Long
  
  

  RaiseEvent OnEvent(eventId)
  
  'Parse individual events
  Select Case gsGetEventType(hEvent)
    Case EVENT_TYPE_APP
      Select Case eventId
        Case EVENT_APP_BEGIN
          RaiseEvent OnAppBegin
        Case EVENT_APP_RUN
          RaiseEvent OnAppRun
        Case EVENT_APP_END
          RaiseEvent OnAppExit
          
        Case EVENT_APP_CLOCK_ROLLBACK
          RaiseEvent OnError(WARN_CLOCK_ROLLBACK, 0, "Clock Rollback Detected")
        Case EVENT_APP_INTEGRITY_CORRUPT
          RaiseEvent OnError(WARN_INTEGRITY_CORRUPT, 0, "Data Integrity Is Corrupted")
      End Select
      
    Case EVENT_TYPE_LICENSE
      Select Case eventId
        Case EVENT_LICENSE_NEWINSTALL
          RaiseEvent OnAppFirstRun
        
        Case EVENT_LICENSE_LOADING
          RaiseEvent OnLicenseLoading
        Case EVENT_LICENSE_READY
          RaiseEvent OnLicenseLoaded
        
        Case EVENT_LICENSE_FAIL
          'extracts more details
          errMsg = PCharToStr(gsGetLicenseErrorEventDetails(hEvent, errCode))
          If errCode = 109 Then
            RaiseEvent OnError(ERROR_FINGERPRINT_MISMATCH, errCode, errMsg)
          Else
            RaiseEvent OnError(ERROR_LICENSE_FAIL, errCode, errMsg)
          End If
      End Select
    
    Case EVENT_TYPE_ENTITY
      hEntity = gsGetEntityEventSource(hEvent)
      entity = gs.Core.getEntityByHandle(hEntity)
      
      Select Case eventId
        Case EVENT_ENTITY_ACCESS_STARTING
          RaiseEvent OnAccessStarting(entity)
        Case EVENT_ENTITY_ACCESS_STARTED
          RaiseEvent OnAccessStarted(entity)
        Case EVENT_ENTITY_ACCESS_ENDING
          RaiseEvent OnAccessEnding(entity)
        Case EVENT_ENTITY_ACCESS_ENDED
          RaiseEvent OnAccessEnded(entity)
        Case EVENT_ENTITY_ACCESS_INVALID
          RaiseEvent OnAccessInvalid(entity)
        Case EVENT_ENTITY_ACCESS_HEARTBEAT
          RaiseEvent OnAccessHeartBeat(entity)
      End Select
    
    Case EVENT_TYPE_USER
      usrDataPtr = gsGetUserEventData(hEvent, usrDataSize)
      RaiseEvent OnUserEvent(eventId, usrDataPtr, usrDataSize)
      
  End Select
  
End Sub

Public Sub Init()
  m_hMonitorCB = gsCreateMonitorEx(AddressOf s_monitorCallback, 0, "$SDK")
End Sub

