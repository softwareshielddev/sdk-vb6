Attribute VB_Name = "TGSHelper"
Option Explicit
'Date Time Helper

Public Type FILETIME
    dwLowDateTime As Long
    dwHighDateTime As Long
End Type

Public Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Public Declare Function FileTimeToSystemTime Lib "kernel32" (lpFileTime As FILETIME, lpSystemTime As SYSTEMTIME) As Long
Public Declare Function SystemTimeToFileTime Lib "kernel32" (lpSystemTime As SYSTEMTIME, lpFileTime As FILETIME) As Long
Public Declare Function LocalFileTimeToFileTime Lib "kernel32" (lpLocalFileTime As FILETIME, lpFileTime As FILETIME) As Long
Public Declare Function FileTimeToLocalFileTime Lib "kernel32" (lpFileTime As FILETIME, lpLocalFileTime As FILETIME) As Long

' Convert a Date into a SYSTEMTIME.
Public Sub DateToSystemTime(ByVal the_date As Date, ByRef _
    system_time As SYSTEMTIME)
    With system_time
        .wYear = Year(the_date)
        .wMonth = Month(the_date)
        .wDay = Day(the_date)
        .wHour = Hour(the_date)
        .wMinute = Minute(the_date)
        .wSecond = Second(the_date)
    End With
End Sub

' Convert a SYSTEMTIME into a Date.
Public Sub SystemTimeToDate(system_time As SYSTEMTIME, _
    ByRef the_date As Date)
    With system_time
        the_date = DateSerial(.wYear, .wMonth, .wDay) + _
                   TimeSerial(.wHour, .wMinute, .wSecond)
    End With
End Sub

' Convert a local time to UTC.
Public Function LocalTimeToUTC(ByVal the_date As Date) As _
    Date
    Dim system_time As SYSTEMTIME
    Dim local_file_time As FILETIME
    Dim utc_file_time As FILETIME

    ' Convert it into a SYSTEMTIME.
    DateToSystemTime the_date, system_time

    ' Convert it to a FILETIME.
    SystemTimeToFileTime system_time, local_file_time

    ' Convert it to a UTC time.
    LocalFileTimeToFileTime local_file_time, utc_file_time

    ' Convert it to a SYSTEMTIME.
    FileTimeToSystemTime utc_file_time, system_time

    ' Convert it to a Date.
    SystemTimeToDate system_time, the_date

    LocalTimeToUTC = the_date
End Function

' Convert a UTC time into local time.
Public Function UTCToLocalTime(ByVal the_date As Date) As Date
    Dim system_time As SYSTEMTIME
    Dim local_file_time As FILETIME
    Dim utc_file_time As FILETIME

    ' Convert it into a SYSTEMTIME.
    DateToSystemTime the_date, system_time

    ' Convert it to a UTC time.
    SystemTimeToFileTime system_time, utc_file_time

    ' Convert it to a FILETIME.
    FileTimeToLocalFileTime utc_file_time, local_file_time

    ' Convert it to a SYSTEMTIME.
    FileTimeToSystemTime local_file_time, system_time

    ' Convert it to a Date.
    SystemTimeToDate system_time, the_date

    UTCToLocalTime = the_date
End Function


Public Sub ParseSeconds(ByVal totalSeconds As Long, ByRef days As Integer, ByRef hours As Integer, ByRef minutes As Integer, ByRef seconds As Integer)
'Convert seconds to date length
  Dim seconds_per_day As Long, seconds_per_hour As Integer, seconds_per_minute As Integer
  
  Dim i As Long
  
  seconds_per_minute = 60
  seconds_per_hour = 3600
  seconds_per_day = 86400
  
   
  i = totalSeconds
   
  If i >= seconds_per_day Then
    days = i \ seconds_per_day
    i = i - days * seconds_per_day
  Else
    days = 0
  End If
  
  If i >= seconds_per_hour Then
    hours = i \ seconds_per_hour
    i = i - hours * seconds_per_hour
  Else
    hours = 0
  End If
  
  If i >= seconds_per_minute Then
    minutes = i \ seconds_per_minute
    seconds = i - minutes * seconds_per_minute
  Else
    minutes = 0
    seconds = i
  End If
End Sub

Public Function ConvertSecondsToString(ByVal totalSeconds As Long) As String
  Dim days As Integer, hours As Integer, minutes As Integer, seconds As Integer
  
  Call ParseSeconds(totalSeconds, days, hours, minutes, seconds)
  
  ConvertSecondsToString = IIf(days > 0, CStr(days) & " d", "")
  ConvertSecondsToString = ConvertSecondsToString & IIf(hours > 0, CStr(hours) & " h ", "")
  ConvertSecondsToString = ConvertSecondsToString & IIf(minutes > 0, CStr(minutes) & " m ", "")
  ConvertSecondsToString = ConvertSecondsToString & IIf(seconds > 0, CStr(seconds) & " s", "")
  ConvertSecondsToString = RTrim$(ConvertSecondsToString)
End Function
