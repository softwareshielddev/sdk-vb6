VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TGSInspector_HardDate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public Enum GS_HardDate_Kind
    EXPIRE_ON_DATE = 1 ' Expire on a date (End_Date)
    VALID_IN_RANGE = 2 'Valid between a date range (Start_Date, End_Date)
    VALID_FROM_DATE = 3 'Will become valid after a date (Start_Date)
End Enum

Private m_lic As TGSLicense



Friend Property Let License(lic As TGSLicense)
'[INTERNAL]
  Set m_lic = lic
End Property


'============================== Public Properties =================================
Friend Property Get Kind() As GS_HardDate_Kind
  If Not m_lic.getParamByName("timeBeginEnabled").ValAsBoolean Then
    Kind = EXPIRE_ON_DATE
  Else
    If m_lic.getParamByName("timeEndEnabled").ValAsBoolean Then
      Kind = VALID_IN_RANGE
    Else
      Kind = VALID_FROM_DATE
    End If
  End If
End Property

Friend Property Get ExpireDate() As Date
'Local Expire date valid, [EXPIRE_ON_DATE] only
  If Kind = EXPIRE_ON_DATE Then
    ExpireDate = m_lic.getParamByName("timeEnd").ValAsLocalDate
  Else
    err.Raise vbObjectError + GSErr.VARIABLE_INVALID_VALUE, "Inspector_HardDate", "ExpireDate is only valid for hard date kind [EXPIRE_ON_DATE]!"
  End If
End Property

Friend Property Get ValidFromDate() As Date
'The beginning of valid date, [VALID_FROM_DATE] only
  If Kind = VALID_FROM_DATE Then
    ValidFromDate = m_lic.getParamByName("timeBegin").ValAsLocalDate
  Else
    err.Raise vbObjectError + GSErr.VARIABLE_INVALID_VALUE, "Inspector_HardDate", "ValidFromDate is only valid for hard date kind [VALID_FROM_DATE]!"
  End If
End Property

Friend Property Get RangeBeginDate() As Date
'Local valid begin date, [VALID_IN_RANGE] only
  If Kind = VALID_IN_RANGE Then
    RangeBeginDate = m_lic.getParamByName("timeBegin").ValAsLocalDate
  Else
    err.Raise vbObjectError + GSErr.VARIABLE_INVALID_VALUE, "Inspector_HardDate", "RangeBeginDate is only valid for hard date kind [VALID_IN_RANGE]!"
  End If
End Property

Friend Property Get RangeEndDate() As Date
'Local valid range end date, [VALID_IN_RANGE] only
  If Kind = VALID_IN_RANGE Then
    RangeEndDate = m_lic.getParamByName("timeEnd").ValAsLocalDate
  Else
    err.Raise vbObjectError + GSErr.VARIABLE_INVALID_VALUE, "Inspector_HardDate", "RangeEndDate is only valid for hard date kind [VALID_IN_RANGE]!"
  End If
End Property

Friend Property Get RollbackTolerance() As Long
'Clock Rollback Tolerance, 0 if disabled
  RollbackTolerance = m_lic.getParamByName("rollbackTolerance").Value
End Property


Friend Property Get TimeUsed() As Long
'How many seconds consumed since valid time
  Dim max_length As Long
  Dim t0, t1 As Date
  
  Select Case Kind
    Case VALID_IN_RANGE
      t0 = m_lic.getParamByName("timeBegin").ValAsLocalDate
      t1 = m_lic.getParamByName("timeEnd").ValAsLocalDate
      
      TimeUsed = DateDiff("s", t0, Now)
      max_length = DateDiff("s", t0, t1)
      If TimeUsed > max_length Then TimeUsed = max_length
    Case VALID_FROM_DATE
      t0 = m_lic.getParamByName("timeBegin").ValAsLocalDate
      TimeUsed = DateDiff("s", t0, Now)
      
    Case EXPIRE_ON_DATE
      err.Raise vbObjectError + GSErr.VARIABLE_INVALID_VALUE, "Inspector_HardDate", "TimeUsed is not valid for hard date kind [EXPIRE_ON_DATE]!"
  End Select
End Property


Friend Property Get TimeLeft() As Long
'How many seconds left before expire
  
  Select Case Kind
    Case EXPIRE_ON_DATE, VALID_IN_RANGE
      TimeLeft = DateDiff("s", Now, m_lic.getParamByName("timeEnd").ValAsLocalDate)
    
    Case VALID_FROM_DATE
      err.Raise vbObjectError + GSErr.VARIABLE_INVALID_VALUE, "Inspector_HardDate", "TimeLeft is not valid for hard date kind [VALID_FROM_DATE]!"
  End Select
End Property

Private Sub Class_Terminate()
  Set m_lic = Nothing
End Sub

