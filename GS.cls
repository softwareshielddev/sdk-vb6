VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TGS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'[INTERNAL ] ******************************* Native Low-Level API *********************************************
Private Declare Function gsGetVersion Lib "gsCore.dll" Alias "#2" () As Long
Private Declare Function gsRunInWrappedMode Lib "gsCore.dll" Alias "#81" () As Byte
Private Declare Function gsRunInsideVM Lib "gsCore.dll" Alias "#82" (ByVal vmMask As Long) As Byte
Private Declare Function gsIsDebugVersion Lib "gsCore.dll" Alias "#91" () As Byte


Public Enum GSErr
    CANNOT_INIT_CORE = 1
    ENTITY_OPEN_FAILURE = 2
    VARIABLE_INVALID_VALUE = 3
    VARIABLE_NOT_FOUND = 4
    LICENSEPARAM_OPEN_FAILURE = 5
    REQUEST_CREATE_FAILURE = 6
    ACTION_CREATE_FAILURE = 7
    MVPKG_CREATE_FAILURE = 8
    MVPKG_UPLOAD_FAILURE = 9
End Enum

'local variable(s) to hold property value(s)
Private mvarProductId As String 'local copy
Private mvarPassword As String 'local copy
Private mvarLicenseFile As String 'local copy

Friend Property Let LicenseFile(ByVal vData As String)
'Full path to the license file
'If it is not a full path, SDK will try to find the license file side by side with the application exe
    mvarLicenseFile = vData
End Property

Friend Property Let Password(ByVal vData As String)
'The main license file's password
    mvarPassword = vData
End Property

Friend Property Let ProductId(ByVal vData As String)
'The license project's product Id
    mvarProductId = vData
End Property


Friend Static Property Get App() As TGSApp
'Singleton App Object (gs.App)
  Dim s_app As TGSApp
  
  If s_app Is Nothing Then
    Set s_app = New TGSApp
  End If
  
  Set App = s_app
End Property

Friend Static Property Get Core(Optional ByVal CreateIfNonExistent As Boolean = True) As TGSCore
'Singleton Core Object (gs.Core)
    Dim s_core As TGSCore
    Dim errCode As Integer
    
    If s_core Is Nothing Then
        Set s_core = New TGSCore
        errCode = s_core.Init(mvarProductId, mvarLicenseFile, mvarPassword)
        If 0 <> errCode Then
            err.Raise vbObjectError + GSErr.CANNOT_INIT_CORE, "gs", s_core.LastErrorMessage
        End If
    End If
        
    Set Core = s_core
End Property

Friend Property Get SDKVersion() As String
'The SDK version being used
  Dim ver As Long
  ver = gsGetVersion()
  SDKVersion = PCharToStr(ver)
End Property

Friend Property Get RunInWrappedMode() As Boolean
'Test if the current process is running inside GS5 Ironwrapper runtime
'Returns false when the running app is not wrapped
  RunInWrappedMode = (gsRunInWrappedMode() <> 0)
End Property

Friend Property Get RunInVM() As Boolean
'    Test if the current process is runing inside any virtual machine (VMWare / VirtualPC / VirtualBox /Fusion /Parallel / QEMU).
  RunInVM = (gsRunInsideVM(-1) <> 0)
End Property

Friend Property Get IsDebugVersion() As Boolean
' Is current SDK binary a debugger version?
  IsDebugVersion = (gsIsDebugVersion() <> 0)
End Property


'[INTERNAL]
Friend Sub cleanUp()
   Dim mycore As TGSCore
      
   Set mycore = Me.Core(False)
   If Not mycore Is Nothing Then
        mycore.cleanUp
   End If
End Sub

