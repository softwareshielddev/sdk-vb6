VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TGSAction"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'TGSAction
Public Enum GSAction
    ' Unlock entity / license
    ACT_UNLOCK = 1
    ' Lock down entity / license
    ACT_LOCK = 2
    'Sets a parameter's value
    ACT_SET_PARAM = 3
    ' Enables a parameter
    ACT_ENABLE_PARAM = 4
    ' Disable a parameter
    ACT_DISABLE_PARAM = 5
    ' Enable Copy protection feature (NodeLock)
    ACT_ENABLE_COPYPROTECTION = 6
    ' Disable Copy protection feature (NodeLock)
    ACT_DISABLE_COPYPROTECTION = 7

    ACT_ENABLE_ALLEXPIRATION = 8
    ACT_DISABLE_ALLEXPIRATION = 9
    ACT_RESET_ALLEXPIRATION = 10

    ' Clean up local license storage
    ACT_CLEAN = 11
    ' Dummy action, carry only client id
    ACT_DUMMY = 12

    ' Activation Code can be used only once
    ACT_ONE_SHOT = 17
    ' Activation Code has a shelf time
    ACT_SHELFTIME = 18

    ' FingerPrint Mismatch Fix
    ACT_FIX = 19

    '** LM-specific actions **
    'LM.expire.accessTime
    ' Increase /Decrease access time (LM.expire.accessTime)
    ACT_ADD_ACCESSTIME = 100
    ' Sets access time (LM.expire.accessTime)
    ACT_SET_ACCESSTIME = 101

    'LM.expire.hardDate
    ' Sets start date (LM.expire.hardDate)
    ACT_SET_STARTDATE = 102
    ' Sets end date (LM.expire.hardDate)
    ACT_SET_ENDDATE = 103

    ' Sets maximum execution session time (LM.expire.sessionTime)
    ACT_SET_SESSIONTIME = 104

    ' LM.expire.period
    ' Sets expire period (LM.expire.period)
    ACT_SET_EXPIRE_PERIOD = 105
    ' Increases / Decreases expire period (LM.expire.period)
    ACT_ADD_EXPIRE_PERIOD = 106

    ' LM.expire.duration
    ' Sets expire duration (LM.expire.duration)
    ACT_SET_EXPIRE_DURATION = 107
    ' Increases / Decreases expire duration (LM.expire.duration)
    ACT_ADD_EXPIRE_DURATION = 108
    
End Enum



Private m_hAct As Long

'[INTERNAL ] ******************************* Native Low-Level API *********************************************


'============================== Internal Properties ====================================
Friend Property Let handle(ByVal hAct As Long)
  m_hAct = hAct
End Property

Friend Property Get handle() As Long
  handle = m_hAct
End Property


Private Sub Class_Terminate()
  Call gsCloseHandle(m_hAct)
End Sub


